/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.admin.model.Dict;
import io.jplus.admin.service.DictService;
import io.jplus.core.web.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.util.List;

/**
 * @author Retire 吴益峰 （372310383@qq.com）
 * @version V1.0
 * @Package io.jplus.admin.controller
 */
@RequestMapping(value = "/admin/dict", viewPath = JplusConsts.BASE_VIEW_PATH + "admin")
public class DictController extends BaseController {

    @RPCInject
    DictService dictService;

    public void index() {
        render("dict.html");
    }

    @RequiresPermissions("sys:dict:list")
    public void list() {
        Page<Dict> page = dictService.queryPage(getQuery());
        renderJsonPage(page);
    }

    @RequiresPermissions("sys:dict:info")
    public void info() {
        String dictId = getPara();
        if (dictId == null) {
            renderJsonForFail("id不能为空");
            return;
        }
        Dict dict = dictService.findById(dictId);
        renderJson(Ret.ok("dict", dict));
    }

    @Before(POST.class)
    @RequiresPermissions("sys:dict:save")
    public void save() {
        Dict dict = jsonToModel(Dict.class);
        boolean tag = dictService.save(dict)==null?false:true;
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("保存失败！");
        }
    }

    @Before(POST.class)
    @RequiresPermissions("sys:dict:update")
    public void update() {
        Dict dict = jsonToModel(Dict.class);
        boolean tag = dictService.update(dict);
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("更新失败！");
        }
    }

    @RequiresPermissions("sys:dict:delete")
    public void delete() {
        List ids = jsonToList();
        if (ids == null) {
            renderJsonForFail("id不能为空！");
            return;
        }
        boolean tag = dictService.deleteById(ids.toArray());
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("删除失败！");
        }
    }

    @RequiresPermissions("sys:dict:copy")
    public void copy() {
        Long dictId = getParaToLong();
        if (dictId == null) {
            renderJsonForFail("id不能为空");
            return;
        }

        boolean tag = dictService.copy(dictId);
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("复制失败！");
        }
    }
}
