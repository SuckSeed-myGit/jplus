/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.admin.model.User;
import io.jplus.admin.service.UserService;
import io.jplus.common.Query;
import io.jplus.core.web.base.BaseController;

import java.util.List;


@RequestMapping(value = "/admin/user", viewPath = JplusConsts.BASE_VIEW_PATH + "admin")
public class UserController extends BaseController {

    @RPCInject
    UserService userService;


    public void index() {
        render("user.html");
    }

    public void list() {
        Page<User> page = userService.queryPage(new Query(getParaMap()));
        renderJsonPage(page);
    }


    @Before(POST.class)
    public void save() {
        User user = jsonToModel(User.class);
        boolean tag = userService.saveUser(user);
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("保存失败！");
        }

    }

    @Before(POST.class)
    public void update() {
        User user = jsonToModel(User.class);
        boolean tag = userService.updateUser(user);
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("更新失败!");
        }
    }

    public void delete() {
        List ids = jsonToList();
        if (ids == null) {
            renderJsonForFail("id不能为空！");
            return;
        }
        boolean tag = userService.deleteById(ids.toArray());
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("删除失败!");
        }
    }

    public void currInfo() {
        User user = getAttr(JplusConsts.JPLUS_USER);
        user.remove("salt","password");
        renderJson(Ret.ok("user", user));
    }

    public void info() {
        String userId = getPara();
        if (userId == null) {
            renderJsonForFail("id不能为空");
            return;
        }
        User user = userService.findUserById(userId);
        renderJson(Ret.ok("user", user));
    }

}
