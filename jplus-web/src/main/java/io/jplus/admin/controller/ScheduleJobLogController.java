package io.jplus.admin.controller;

import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.core.web.base.BaseController;

@RequestMapping(value = "/admin/scheduleLog", viewPath = JplusConsts.BASE_VIEW_PATH + "admin")
public class ScheduleJobLogController extends BaseController {

    public void index() {
        render("schedule_log.html");
    }
}
