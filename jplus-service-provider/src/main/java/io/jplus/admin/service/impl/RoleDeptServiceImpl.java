package io.jplus.admin.service.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.RoleDept;
import io.jplus.admin.service.RoleDeptService;

import java.util.List;


@Bean
@RPCBean
public class RoleDeptServiceImpl extends JbootServiceBase<RoleDept> implements RoleDeptService {

    @Override
    public List<RoleDept> findListByRoleId(Object roleId) {
        Columns columns = Columns.create();
        columns.eq("role_id", roleId);
        List<RoleDept> roleDeptList = DAO.findListByColumns(columns);
        return roleDeptList;
    }

    @Override
    public boolean deleteByRoleId(Object id) {
        SqlPara sqlPara = Db.getSqlPara("admin-roledept.deleteByRoleId");
        sqlPara.addPara(id);
        return Db.update(sqlPara) >0 ? true : false;
    }

    @Override
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }
}